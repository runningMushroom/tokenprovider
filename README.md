# Functionality
* Create JWT Token with claims: [TokenType, IdNumber, SystemId, Email, Mobile, Password, Role]
* Validate a token
* Get claims from token
* Validate claims against token claims

# Target Framework
* netcoreapp3.1

# Instructions
* Add TokenSettings to appsettings.json
    * "TokenSettings": { "Secret": "Your secret string", "Issuer": "Issuer", "Audience": "Audience"}
* Add services.ConfigureTokenProvider(Configuration) to your Startup.cs file
* Inject & use

# TODO
* Create detailed description on how to use this repo - comin soon
