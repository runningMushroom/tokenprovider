﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TokenProvider
{
    public class ClaimsDto
    {
        public string TokenType { get; set; }
        public string IdNumber { get; set; }
        public string SystemId { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}