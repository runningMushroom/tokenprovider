﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace TokenProvider
{
    public static class TokenProviderExtension
    {
        public static void ConfigureTokenProvider(this IServiceCollection services, IConfiguration config)
        {
            //services.AddSingleton(config.GetSection("TokenSettings").Get<TokenSettingsDto>());
            services.AddScoped<ITokenProvider, TokenProviderRepo>();
        }
    }
}