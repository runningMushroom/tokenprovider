//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: System.Reflection.AssemblyCompanyAttribute("runningMushroom")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Release")]
[assembly: System.Reflection.AssemblyDescriptionAttribute("Creates tokens used for email & one time login")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("1.0.3.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("1.0.3")]
[assembly: System.Reflection.AssemblyProductAttribute("Running.Mushroom.TokenProvider")]
[assembly: System.Reflection.AssemblyTitleAttribute("TokenProvider")]
[assembly: System.Reflection.AssemblyVersionAttribute("1.0.3.0")]

// Generated by the MSBuild WriteCodeFragment class.

