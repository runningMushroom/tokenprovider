﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace TokenProvider
{
    public class TokenProviderRepo : ITokenProvider
    {
        public readonly TokenSettingsDto _settings;

        public TokenProviderRepo()
        {
            var config = new ConfigurationBuilder();
            // Get current directory will return the root dir of Base app as that is the running application
            var path = Path.Join(Directory.GetCurrentDirectory(), "appsettings.json");
            config.AddJsonFile(path, false);
            var root = config.Build();
            _settings = root.GetSection("TokenSettings").Get<TokenSettingsDto>();
        }

        public TokenSettingsDto settings { get => _settings; }

        public string CreateToken(ClaimsDto claims, int lifeSpan)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                     new Claim("TokenType", claims.TokenType),
                     new Claim("IdNumber", claims.IdNumber),
                     new Claim("SystemId", claims.SystemId),
                     new Claim("Email", claims.Email),
                     new Claim("Mobile", claims.Mobile),
                     new Claim("Password", claims.Password),
                     new Claim("Role", claims.Role)
                }),
                Expires = DateTime.UtcNow.AddHours(lifeSpan),
                Issuer = _settings.Issuer,
                Audience = _settings.Issuer,
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_settings.Secret)),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var handler = new JwtSecurityTokenHandler();
            var token = handler.CreateToken(tokenDescriptor);
            return handler.WriteToken(token);
        }

        public bool ValidateToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            try
            {
                handler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = _settings.Issuer,
                    ValidAudience = _settings.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_settings.Secret))
                }, out SecurityToken validatedToken);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public ClaimsDto GetClaimsFromToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var tokenDetails = handler.ReadToken(token) as JwtSecurityToken;

            return new ClaimsDto()
            {
                //TokenType = tokenDetails.Claims.First(c => c.Properties.ContainsKey("TokenType")).Value,
                TokenType = tokenDetails.Claims.First(c => c.Type == "TokenType").Value,
                IdNumber = tokenDetails.Claims.First(c => c.Type == "IdNumber").Value,
                SystemId = tokenDetails.Claims.First(c => c.Type == "SystemId").Value,
                Email = tokenDetails.Claims.First(c => c.Type == "Email").Value,
                Mobile = tokenDetails.Claims.First(c => c.Type == "Mobile").Value,
                Password = tokenDetails.Claims.First(c => c.Type == "Password").Value,
            };
        }

        public bool ValidateClaims(ClaimsDto validClaims, ClaimsDto tokenClaims)
        {
            foreach (var p in validClaims.GetType().GetProperties())
            {
                if (p.GetValue(validClaims) != tokenClaims.GetType().GetProperty(p.Name).GetValue(tokenClaims))
                    return false;
            }
            return true;
        }
    }
}