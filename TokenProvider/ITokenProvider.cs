﻿namespace TokenProvider
{
    public interface ITokenProvider
    {
        /// <summary>
        /// Create JWT Token.
        /// Lifespan is the amount of hours this token will be valid.
        /// </summary>
        /// <param name="claims"></param>
        /// <param name="lifeSpan"></param>
        /// <returns>string</returns>
        string CreateToken(ClaimsDto claims, int lifeSpan);

        /// <summary>
        /// Validates wether a token was created by TokenProvider.CreateTokn
        /// </summary>
        /// <param name="token"></param>
        /// <returns>bool</returns>
        bool ValidateToken(string token);

        /// <summary>
        /// Retreives claims from a jwt token created by TokenProvider.CreateTokn
        /// </summary>
        /// <param name="token"></param>
        /// <param name="claims"></param>
        /// <returns>ClaimsDto</returns>
        ClaimsDto GetClaimsFromToken(string token);

        /// <summary>
        ///
        /// </summary>
        /// <param name="validClaims"></param>
        /// <param name="tokenClaims"></param>
        /// <returns>bool</returns>
        bool ValidateClaims(ClaimsDto validClaims, ClaimsDto tokenClaims);
    }
}